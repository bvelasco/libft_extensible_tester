# Libft Extensible Tester
## Introducción
Libft Extensible Tester es un tester (programa automatizado de testeo) de "LIBFT" diseñado para ser extensible y poderse aplicar a distintas actualizaciones
### Método de creación
Libft es un proyecto de 42, de hecho, es le primero, su función es simple, crear unas funciones aprovechando lo aprendido durante la piscina que te serán utiles durante una parte del cursus, el proyecto ya ha sido entregado por mi pero con la intención de hacer esto con suficiente calidad se ha creado un remake llamado <a href="https://gitlab.com/bvelasco/libft_remake">**Libft_remake**</a> este remake busca mejorar la libft para poder competir en velocidad de ejecución con la anterior versión y, en ultima instancia, con la libft de otros estudiantes.
### Licencia
Commo todos los proyectos publicos de mi gitlab relacionados con 42 la licencia de este tester es Public Domain (sin restricciones) considero que cualquiera tiene derecho a usarlo y a modificarlo a su gusto siempre que cumpla las normas de 42.